# Adapted from source: https://github.com/mrts/docker-cmake-gtest-valgrind-ubuntu/blob/master/Dockerfile

FROM ubuntu:22.04

# Install Ubuntu packages, as each RUN commits the layer to image, need to
# chain commands and clean up in the end to keep the image small
RUN apt update && \
    apt install -y --no-install-recommends \
        git \
        build-essential \
        cmake \
        libgtk-4-dev \
        libgtest-dev \
        lcov && \
    apt clean && \
    rm -rf /var/lib/apt/lists

# Build GTest library
RUN cd /usr/src/googletest && \
    cmake . && \
    cmake --build . --target install

# Create non-privileged user
RUN useradd -ms /bin/bash dockeruser
USER dockeruser
RUN mkdir /home/dockeruser/bldc_controller_simulator
WORKDIR /home/dockeruser/bldc_controller_simulator

# Copy application files into container
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/cmake ./cmake
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/config ./config
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/gtk_ui_builder ./gtk_ui_builder
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/include ./include
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/src ./src
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/tests ./tests
COPY --chown=dockeruser:dockeruser bldc_controller_simulator/CMakeLists.txt .

# Compile
RUN cmake -D CMAKE_BUILD_TYPE=DEBUG -S . -B build && \
    cmake --build build


# # Run test executables --> Moved to test stage in .gitlab-ci.yml

# # ## If coverage set up
# # RUN cd build && \
# #     make && \
# #     make bldc_controller_simulator_coverage

# ## Only tests wo coverage
# RUN cd build/tests/ && \
#     ./test_gtest





