# BLDC Motor Controller - Security Project

## Introduction
This project involves revisiting and enhancing a Brushless DC (BLDC) motor controller. The initial controller is based on a simple trapezoidal architecture and is implemented in the C language. Our goal is to make this controller safe ("SAFE") for various applications.

## Objective
Our main objective is to understand the basic principles of BLDC motor control and apply them to create a secure controller. We will need to rethink the controller's architecture and complete its implementation to meet specific safety requirements.

## Development Environment
No real hardware is required for this project. We will have access to a simulator with a basic graphical interface. This simulator will allow us to test our controller without the need for physical hardware. The simulator code is provided in the form of a DLL, which means we will not have access to its internal workings.

## Simulator and Fault Injection
The simulator is equipped with a fault injector that will allow us to simulate various failure scenarios. We will be able to test our controller under simulated fault conditions, such as motor overheating or electronic malfunctions.

## Project Contents
The project will include the following elements:
- A basic trapezoidal controller example in the C language.
- A simulator for testing the controller.
- A fault injector for simulating failure scenarios.
- Detailed instructions on tasks to be completed and safety criteria to be met.

## Version
Version 1.0

## Note
This README provides us with an overview of the project and its objectives. Detailed instructions on setting up the project and tasks to be completed will be provided separately.
