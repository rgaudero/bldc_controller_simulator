#! /bin/bash
# calling convention
# checkmisragui <folder>

project=$1
cppreportdir=$project/cppreport
mkdir -p $cppreportdir

#With MISRA add-on
cppcheck $1 --addon=./misra.json --enable=all --std=c11 main.c --xml 2> $cppreportdir/report.xml
cppcheck-htmlreport --file=$cppreportdir/report.xml --title=CppcheckTest --report-dir=$cppreportdir
