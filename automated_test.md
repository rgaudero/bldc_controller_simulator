## Implemented Tests

### Unitary Tests

#### Test 1

**Overview:**
This test evaluates the motor's response when the temperature exceeds 145 degrees for over 100 milliseconds.

**Detailed Explanation:**
- **Initial Condition:** The motor simulation runs normally.
- **Action:** At 1.3 seconds into the simulation, the "temp" variable is increased to 150 degrees.
- **Function Tested:** `check_temp_to_sto()`
- **Expected Outcome:** The "current_mode" should change to STO_MODE, indicating that the motor has entered a safe state due to the high temperature.

**Mocks et Isolation:**
Pour que ce test soit un véritable test unitaire isolé, il est nécessaire de mocker les fonctions et les variables globales qui ne font pas partie de la fonction testée mais qui interagissent avec elle:

- **Mocker les capteurs de température:** la fonction check_temp_to_sto() dépend d'une fonction qui lit la température actuelle, cette fonction doit être mockée.
- **Mocker le temps ou le timer:** la vérification de la durée (100 millisecondes) dépend d'une variable globale "sim_time" qui mesure le temps, cette variable doit également être mockée.

#### Test 2

**Overview:**
This test checks the motor's reaction when the "target_rpm" changes but there is no corresponding change in "actual_rpm".

**Detailed Explanation:**
- **Initial Condition:** The motor simulation runs with synchronized "target_rpm" and "actual_rpm".
- **Action:** After 2.2 seconds, the "target_rpm" is changed to 1500 rpm, but the "actual_rpm" remains unchanged.
- **Function Tested:** `check_change_target_rpm_reaction_actual_rpm()`
- **Expected Outcome:** The "current_mode" should switch to STO_MODE, reflecting a fault condition due to the discrepancy between the target and actual RPMs.

**Mocks et Isolation:**
Pour ce test également, il est nécessaire de mocker les dépendances :
- **Mocker les capteurs de RPM:** Les fonctions qui lisent les valeurs de "target_rpm" et "actual_rpm" doivent être mockées.
- **Mocker le temps ou le timer:** la vérification de la durée (200 millisecondes) dépend d'une variable globale "sim_time" qui mesure le temps, cette variable doit également être mockée.

#### Test 1

**Overview:**
This test assesses the motor's behavior when there is a significant deviation between "calculated_rpm" and "actual_rpm".

**Detailed Explanation:**
- **Initial Condition:** The motor runs with synchronized RPM values.
- **Action:**
  - Increment the "motor_quant.target_rpm" by 10 every simulation step until reaching 1500 rpm.
  - At 2.0 seconds, set the "actual_rpm" to 2500 rpm to create a deviation exceeding 20% between "calculated_rpm" and "actual_rpm".
- **Functions Tested:** `check_deviation_calculated_actual_rpm()` and `get_calculated_rpm()`
- **Expected Outcomes:**
  - The "calculated_rpm" should be within the range of 1450 to 1550 rpm by the end of the simulation, as the "target_rpm" is capped at 1500 rpm.
  - The "current_mode" should transition to STO_MODE, indicating a significant deviation fault.

<p align = "center">
 <img src = "images/integrated_1.png" width="600">
</p>

#### Test 2

**Overview:**
This test verifies the motor's operation when the temperature exceeds 100 degrees, focusing on RPM limitations in DEGRADED_MODE.

**Detailed Explanation:**
- **Initial Condition:** The motor runs normally with increasing RPMs.
- **Action:**
  - Increment the "motor_quant.target_rpm" by 10 every simulation step until it reaches 5000 rpm.
  - After 0.7 seconds, increment the "temp" by 1 degree each step until it exceeds 100 degrees.
- **Functions Tested:** `check_temp_to_deg()` and `limit_rpm`
- **Expected Outcomes:**
  - The "temp" should exceed 100 degrees.
  - The "target_rpm" and "actual_rpm" should be limited to 4000 rpm, though oscillations might cause slight deviations (testing checks for values up to 4010 for "target_rpm" and 4200 for "actual_rpm").
  - The "current_mode" should change to DEGRADED_MODE, indicating a precautionary state due to high temperature.

<p align = "center">
 <img src = "images/integrated_2.png" width="600">
</p>

## Conceptual System Test

### Overview:
The system test evaluates the integrated functionality of the motor control system based on the provided specifications and implemented functions. The test ensures that the system meets the performance and safety requirements specified, transitioning correctly between different operational modes.

### Detailed Explanation:

#### Initial Conditions:
- The system starts in RUNNING_Mode.
- All sensor values (actual RPM, target RPM, temperature, HALL1, torque) are within normal operating ranges.
- Logging is enabled to track mode changes and reasons.

#### Actions and Expected Outcomes:
1. **Measurement Interval Verification:**
   - **Action:** Measure actual RPM, target RPM, temperature, HALL1 sensor value, and torque.
   - **Functions Tested:** `get_actual_rpm()`, `get_target_rpm()`, `get_temp()`, `get_hall1()`, `get_torque()`
   - **Expected Outcome:** All measurements are logged within their respective maximum intervals (50ms, 50ms, 100ms, 1µs, 10µs).

2. **Target RPM Change Reaction:**
   - **Action:** Change the target RPM and observe the actual RPM.
   - **Function Tested:** `check_change_target_rpm_reaction_actual_rpm()`
   - **Expected Outcome:** The system enters STO_Mode if no reaction is observed on actual RPM for more than 200ms after the target RPM change.

3. **Temperature-Based Mode Transitions:**
   - **Action:** Gradually increase the temperature above 100°C, then above 145°C.
   - **Functions Tested:** `check_temp_to_deg()`, `check_temp_to_sto()`
   - **Expected Outcome:**
     - The system enters DEGRADED_Mode if the temperature exceeds 100°C for more than 100ms.
     - The system enters STO_Mode if the temperature exceeds 145°C for more than 100ms.

4. **Degraded Mode to STO Mode Transition:**
   - **Action:** Maintain the system in DEGRADED_Mode for 2000ms.
   - **Function Tested:** `check_to_sto()`
   - **Expected Outcome:** The system enters STO_Mode after spending 2000ms in DEGRADED_Mode.

5. **Returning to RUNNING_Mode:**
   - **Action:** Decrease the temperature below 90°C.
   - **Function Tested:** `check_temp_to_run()`
   - **Expected Outcome:** The system returns to RUNNING_Mode after the temperature remains below 90°C for 200ms.

6. **RPM Limitation in DEGRADED_Mode:**
   - **Action:** Increase target RPM to above 4000 rpm while in DEGRADED_Mode.
   - **Function Tested:** `limit_rpm()`
   - **Expected Outcome:** The target RPM is limited to 4000 rpm in DEGRADED_Mode.

7. **Start/Stop Input Reaction:**
   - **Action:** Change the Start/Stop input to low and observe the RPM.
   - **Function Tested:** `check_change_button_reaction_rpm()`
   - **Expected Outcome:** The system enters STO_Mode if the RPM doesn't decrease by at least 50% within 2000ms.

8. **Calculated RPM Accuracy:**
   - **Action:** Calculate motor RPM using HALL1 sensor information.
   - **Function Tested:** `get_calculated_rpm()`
   - **Expected Outcome:** The calculated RPM is stored accurately in the "Calculated_RPM" variable.

9. **Calculated vs. Actual RPM Deviation:**
   - **Action:** Introduce a deviation between calculated RPM and actual RPM.
   - **Function Tested:** `check_deviation_calculated_actual_rpm()`
   - **Expected Outcome:** The system enters STO_Mode if there is a deviation of more than 10%.

10. **Torque Oscillation:**
    - **Action:** Introduce an oscillation in the torque input.
    - **Function Tested:** `check_oscillation_torque()`
    - **Expected Outcome:** The system enters STO_Mode if an oscillation of more than 5% is measured for more than 1ms.

11. **Logging Mode Changes:**
    - **Action:** Transition between different modes (RUNNING_Mode, DEGRADED_Mode, STO_Mode).
    - **Function Tested:** `fprintf()`
    - **Expected Outcome:** All mode changes are logged with details on the reasons for the changes.

12. **Testing Transition from STO_Mode to RUNNING_Mode:**
    - **Action:** Maintain the system in STO_Mode for 2000ms.
    - **Function Tested:** `check_to_run()`
    - **Expected Outcome:** For testing purposes, the system enters RUNNING_Mode after being in STO_Mode for 2000ms.

### Test Execution:
The test will be executed in a simulated environment to ensure all conditions and transitions are adequately covered. Logging will be monitored to verify that mode changes and reasons are correctly recorded.

### Summary:
This system test comprehensively covers all the specified requirements and implemented functions, ensuring the motor control system operates correctly under various conditions and transitions between modes as expected.
