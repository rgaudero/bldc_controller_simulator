

# Continuous Integration (CI) Approach: Trunk-Based Development

## Introduction
In this project, we have adopted a Trunk-Based Development approach for managing our Git workflow. Trunk-Based Development is a version control strategy where all developers work primarily on the main branch, and feature branches are created as needed but merged regularly into the main branch.

<p align = "center">
 <img src = "images/Trunk-Based-dev.png" width="200">
</p>

## Strategy
Our Git workflow is structured as follows:

- **Main Branch ("main"):** This is our primary development branch where ongoing development takes place. All developers contribute their changes directly to this branch or through feature branches.

- **Release Branch ("production"):** We maintain a separate release branch named "production" from which stable releases are made. This branch is used for deploying production-ready code.

- **Feature Branches ("xx-dev"):** Each developer creates their own feature branch prefixed with their initials followed by "-dev" (e.g., "Tom-dev"). Feature branches are used for implementing new features or making changes to existing functionality. Developers work on these branches independently and merge them regularly into the main branch.

## Rationale
Our decision to use Trunk-Based Development is based on several key factors:

### Simplicity
Trunk-Based Development offers a simple and straightforward workflow. With developers primarily working on the main branch and creating feature branches as needed, we avoid the complexity of maintaining multiple long-lived branches.

### Continuous Integration
Trunk-Based Development aligns well with the principles of Continuous Integration (CI). By regularly merging feature branches into the main branch, we ensure that new code is continuously integrated, tested, and validated against the latest version of the application. This promotes early detection of integration issues and helps maintain a high level of code quality.

### Rapid Feedback
With Trunk-Based Development, changes are quickly integrated into the main branch and deployed to production. This rapid feedback loop allows us to communicate with each other regularly, enabling us to iterate and improve the application more effectively.

### Reduced Merge Complexity
By merging feature branches into the main branch regularly, we reduce the occurrence of large, complex merge conflicts that can arise in more traditional branching models. This results in a smoother and more streamlined development process.

## Conclusion
In summary, our adoption of Trunk-Based Development, with a main branch for ongoing development and a release branch ("Production") for stable releases, reflects our commitment to simplicity, continuous integration, rapid feedback, and efficient collaboration. 
