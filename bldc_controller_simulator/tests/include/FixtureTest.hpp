/**--------------------------------------------------------------------------
 * @file     test_implemented.cpp
 * @author   Rayan Gauderon
 * @date     Mai, 2024
 * @brief    Test fixture for test setup and teardown.
 *
 * ---------------------------------------------------------------------------
 * Architecture Software
 * EPFL - École Polytechnique Fédérale de Lausanne
 * Rte Cantonale
 * CH-1015 Lausanne
 * Switzerland
 * ---------------------------------------------------------------------------*/

#pragma once

#include <gtest/gtest.h>

#include "stdio.h"

extern "C"
{
    #include "bldc_controller_simulator/students_code.h"
    #include "bldc_controller_simulator/error.h"
    #include "bldc_controller_simulator/simulation.h"
    #include "bldc_controller_simulator/pid.h"
    #include "bldc_controller_simulator/log.h"
    #include "bldc_controller_simulator/custom_typedefs.h"
}


class FixtureTest : public ::testing::Test
{
protected:
    FixtureTest();
    ~FixtureTest();

    controllerIo *controller_io;
    motorQuantities motor_quant;
    pwmMosfets bridge_signals;
    hallSensors hall_sensors;
    bool sto_active_low = true;
    bool enable;
    double sim_time;
    double sim_duration;
    int scenario;
    int status;

    FILE *file_test_1;
    FILE *file_test_2;
    FILE *file_test_3;
    FILE *file_test_4;

    current_mode_t current_mode;
    current_mode_t previous_mode;

    double torque;
    double temp;
    bool hall1;
    double target_rpm;
    double actual_rpm;
    double calculated_rpm;
};

/**
 * @brief Setup function, called each time before every test case is executed.
 */
FixtureTest::FixtureTest(/* args */)
{
    // Set up what you want to set up every time you use this fixture.
    log_init();

    sim_time = 0.0;
    sim_duration = 3.0;


    current_mode = RUNNING_MODE;
    previous_mode = NONE;

    controller_io = new controllerIo();
}

/**
 * @brief Teardown function, called each time after every test case is executed.
 */
FixtureTest::~FixtureTest()
{
    // Clean up after yourself, memory etc.
   
    log_close();
}

