/**--------------------------------------------------------------------------
 * @file     test_implemented.cpp
 * @author   Rayan Gauderon
 * @date     Mai, 2024
 * @brief    Integration test for simulation part of application.
 *
 * ---------------------------------------------------------------------------
 * Architecture Software
 * EPFL - École Polytechnique Fédérale de Lausanne
 * Rte Cantonale
 * CH-1015 Lausanne
 * Switzerland
 * ---------------------------------------------------------------------------*/

#include <gtest/gtest.h>
#include "FixtureTest.hpp"

extern "C"
{
    #include "bldc_controller_simulator/students_code.h"
    #include "bldc_controller_simulator/error.h"
    #include "bldc_controller_simulator/simulation.h"
    #include "bldc_controller_simulator/pid.h"
    #include "bldc_controller_simulator/log.h"
    #include "bldc_controller_simulator/custom_typedefs.h"
}

/********************************************************
* UNITARY TESTS
********************************************************/

/**
* @brief    Test the simulation of the motor with a high temperature.
* @note     The test checks that the mode is switched to STO_MODE when the temperature is too high.
**/
TEST_F(FixtureTest, TestHighTempToDeg)
{
    file_test_1 = fopen("t1_high_temp_to_deg.txt", "w");
    temp = 70.0;

    while (sim_time < sim_duration)
    {  
        sim_time += RK_NUMBER_STEPS * RK_STEP_SIZE;

        //increment temp to > 145 degrees
        if(sim_time > 1.2)
        {
            temp = 150.0;
        }
        current_mode = check_temp_to_sto(current_mode, temp, sim_time, file_test_1);
    }

    EXPECT_EQ(current_mode, STO_MODE);
}

/**
* @brief    Test the simulation of the motor with a change in target_rpm and no reaction in actual_rpm.
* @note     The test checks that the mode is switched to STO_MODE when the actual_rpm is not reacting to the change in target_rpm.
**/
TEST_F(FixtureTest, TestTargetReactionActualRPM)
{
    file_test_2 = fopen("t2_target_reaction_actual_rpm.txt", "w");

    while (sim_time < sim_duration)
    {  
        sim_time += RK_NUMBER_STEPS * RK_STEP_SIZE;

        //target_rpm is changed but not actual_rpm
        if(sim_time > 2.2)
        {
            target_rpm = 1500;
        }
        current_mode = check_change_target_rpm_reaction_actual_rpm(current_mode, actual_rpm, target_rpm, sim_time, file_test_2);
    }

    EXPECT_EQ(current_mode, STO_MODE);
} 
/********************************************************
* END UNITARY TESTS
********************************************************/

/********************************************************
*  INTEGRATION TESTS
********************************************************/

/**
* @brief    Test the simulation of the motor with a deviation between the calculated rpm and the actual rpm.
* @note     The test checks that the mode is switched to STO_MODE when the deviation is too high.
**/
TEST_F(FixtureTest, TestHall1DeviationCalculatedRPMActualRPM)
{
    file_test_3 = fopen("t3_hall1_deviation_calculated_rpm_actual_rpm.txt", "w");

   status = run_simulation_step(&bridge_signals, sto_active_low, scenario, 0, &motor_quant, &hall_sensors);
    if (status != SUCCESS)
    {
        sim_time = sim_duration;
    }
     while (sim_time < sim_duration)
    {
        status = run_pid(motor_quant.target_rpm, motor_quant.actual_rpm, &bridge_signals.pwm);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_inverter_mucontroller(&hall_sensors, &bridge_signals, sto_active_low);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_simulation_step(&bridge_signals, sto_active_low, scenario, sim_time, &motor_quant, &hall_sensors);
        if (status != SUCCESS)
        {
            break;
        }

        if (LOG_STUDENT_IO)
        {
            log_student_io(&bridge_signals, sto_active_low, &motor_quant, &hall_sensors, enable, sim_time);
        }

        //increment target_rpm until 1500 rpm
        if(motor_quant.target_rpm < 1500.0)
        {
            motor_quant.target_rpm += 10.0;
        }


        sim_time += RK_NUMBER_STEPS * RK_STEP_SIZE;

        hall1 = get_hall1(&hall_sensors.h1);

        calculated_rpm = get_calculated_rpm(hall1);

        actual_rpm = get_actual_rpm(&motor_quant.actual_rpm, sim_time);

        //change actual_rpm and not calculated_rpm
        if (sim_time > 2.0)
        {
            actual_rpm = 2500.0;;
        }

        current_mode = check_deviation_calculated_actual_rpm(current_mode, calculated_rpm, actual_rpm, sim_time, file_test_3);
    }

    //check if the calculated rpm has been well calculated
    EXPECT_GT(calculated_rpm, 1450.0);
    EXPECT_LT(calculated_rpm, 1550.0);

    //check if the mode has been switched to STO_MODE
    EXPECT_EQ(current_mode, STO_MODE);
}

/**
* @brief    Test the simulation of the motor with a high temperature and a limited target_rpm.
* @note     The test checks that the mode is switched to DEGRADED_MODE when the temperature is too high and the target_rpm is limited.
**/
TEST_F(FixtureTest, TestHighTempToStoLimitRPM)
{
    file_test_4 = fopen("t4_high_temp_to_sto_limit_rpm.txt", "w");
    temp = 70.0;
    motor_quant.target_rpm = 2000.0;

    status = run_simulation_step(&bridge_signals, sto_active_low, scenario, 0, &motor_quant, &hall_sensors);
    if (status != SUCCESS)
    {
        sim_time = sim_duration;
    }
     while (sim_time < sim_duration)
    {
        status = run_pid(motor_quant.target_rpm, motor_quant.actual_rpm, &bridge_signals.pwm);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_inverter_mucontroller(&hall_sensors, &bridge_signals, sto_active_low);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_simulation_step(&bridge_signals, sto_active_low, scenario, sim_time, &motor_quant, &hall_sensors);
        if (status != SUCCESS)
        {
            break;
        }

        if (LOG_STUDENT_IO)
        {
            log_student_io(&bridge_signals, sto_active_low, &motor_quant, &hall_sensors, enable, sim_time);
        }

        sim_time += RK_NUMBER_STEPS * RK_STEP_SIZE;

        if(motor_quant.target_rpm < 5000.0)
        {
            motor_quant.target_rpm += 10.0;
        }

        //increment temp until 110 degrees
        if((sim_time > 0.7) && (temp < 110.0))
        {
            temp += 1.0;
        }

        actual_rpm = get_actual_rpm(&motor_quant.actual_rpm, sim_time);

        current_mode = check_temp_to_deg(current_mode, temp, sim_time, file_test_4);

        motor_quant.target_rpm = limit_rpm(&motor_quant.target_rpm);

    }

    //check if the temp is above 100 degrees
    EXPECT_GT(temp, 100.0);
    //check if the target_rpm has been limited to 4000 rpm
    EXPECT_LT(target_rpm, 4010.0);
    //check if the actual_rpm has been limited to 4200 rpm (oscillation so it can be higher than target_rpm)
    EXPECT_LT(actual_rpm, 4200.0);
    //check if the mode has been switched to DEGRADED_MODE
    EXPECT_EQ(current_mode, DEGRADED_MODE);
}

/********************************************************
* END INTEGRATION TESTS
********************************************************/