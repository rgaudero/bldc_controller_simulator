/**--------------------------------------------------------------------------
 * @file     students_code.h
 * @author   Urban Willi
 * @date     March, 2024
 * @brief    The trapezoidal controller loop and call of simulation. Students will edit this file.
 * 
 * 
 * ---------------------------------------------------------------------------
 * Robust and Safe Systems Center Fribourg
 * HES-SO - Haute école d’ingénierie et d’architecture Fribourg HEIA Fribourg
 * Passage du Cardinal 13B / Halle Bleue
 * CH-1700 Fribourg
 * Switzerland
 * ---------------------------------------------------------------------------*/

#pragma once

#include <stdio.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "bldc_controller_simulator/error.h"
#include "bldc_controller_simulator/simulation.h"
#include "bldc_controller_simulator/pid.h"
#include "bldc_controller_simulator/log.h"
#include "bldc_controller_simulator/custom_typedefs.h"
#include "bldc_controller_simulator/bldc_controller_simulator_window.h"

/********************************************************
* OUR STRUCTURES
********************************************************/

typedef enum current_mode_t {
    RUNNING_MODE,
    DEGRADED_MODE,
    STO_MODE,
    NONE,
} current_mode_t;

/********************************************************
* END OUR STRUCTURES
********************************************************/

/********************************************************
* OUR FUNCTIONS
********************************************************/

double get_actual_rpm(const double *actual_rpm, double sim_time);

double get_target_rpm(const double *target_rpm, double sim_time);

double get_temp(const double *temp, double sim_time);

bool get_hall1(const bool *h1);

double get_torque(const double *torque, double sim_time);

double get_calculated_rpm(bool h1);

current_mode_t check_change_target_rpm_reaction_actual_rpm(current_mode_t current_mode, double actual_rpm, double target_rpm, double sim_time, FILE *file);

current_mode_t check_temp_to_sto(current_mode_t current_mode, double temp, double sim_time, FILE *file);

current_mode_t check_change_button_reaction_rpm(current_mode_t current_mode, double actual_rpm, bool enable, FILE *file);

current_mode_t check_deviation_calculated_actual_rpm(current_mode_t current_mode, double calculated_rpm, double actual_rpm, double sim_time, FILE *file);

current_mode_t check_oscillation_torque(current_mode_t current_mode, double torque, double actual_rpm, double target_rpm, double sim_time, FILE *file);

current_mode_t check_temp_to_deg(current_mode_t current_mode, double temp, double sim_time, FILE *file);

current_mode_t check_to_sto(current_mode_t current_mode, double current_degraded_time, double sim_time, FILE *file);

current_mode_t check_temp_to_run(current_mode_t current_mode, double temp, double sim_time, FILE *file);

double limit_rpm(double *target_rpm);

current_mode_t check_to_run(current_mode_t current_mode, double current_sto_time, double sim_time, FILE *file);




/********************************************************
* END OUR FUNCTIONS
********************************************************/
void *run_trapezoidal_controller(void *arg);
int update_values_gui(void *arg);
int set_initial_values_controller_io(BldcControllerSimulatorWindow *win);
void set_actual_rpm_label(BldcControllerSimulatorWindow *win, double raw_val);
void sim_cleanup();