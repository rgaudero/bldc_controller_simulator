/**--------------------------------------------------------------------------
 * @file     students_code.c
 * @author   Urban Willi
 * @date     March, 2024
 * @brief    The trapezoidal controller loop and call of simulation. Students will edit this file.
 *
 *
 * ---------------------------------------------------------------------------
 * Robust and Safe Systems Center Fribourg
 * HES-SO - Haute école d’ingénierie et d’architecture Fribourg HEIA Fribourg
 * Passage du Cardinal 13B / Halle Bleue
 * CH-1700 Fribourg
 * Switzerland
 * ---------------------------------------------------------------------------*/

#include <stdio.h>
#include "bldc_controller_simulator/students_code.h"

/********************************************************
* OUR DEFINES
********************************************************/

// Time conversion
#define SEC_TO_US 1000000.0f
#define US_TO_SEC 0.000001f

// Measurement intervals
#define TEMP_MEASUREMENT_INTERVAL 100000 //100ms
#define TORQUE_MEASUREMENT_INTERVAL 10 //10us
#define RPM_MEASUREMENT_INTERVAL 50000 //50ms
#define STABILIZE_RPM 0.3f //300ms

//Transitions between mode
#define TRANSITION_MODE 2.0f //2000ms
#define TRANSITION_DEGRADED_TO_RUNNING_TEMP 0.2f //200ms
#define TRANSITION_RUNNING_TO_DEGRADED_TEMP 0.1f //100ms
#define TRANSITION_TO_STO_TEMP 0.1f //100ms
#define TRANSITION_TO_STO_RPM 0.2f //200ms
#define TRANSITION_TO_STO_SS_BUTTON 2.0f //2000ms


// Safety limits
#define TARGET_RPM_DEGRADED_LIMIT 4000.0f
#define TEMP_RUNNING_TO_DEGRADED 100.0f
#define TEMP_DEGRADED_TO_RUNNING 90.0f
#define TEMP_TO_STO 145.0f
#define COEFF_RPM 0.00007f

/********************************************************
* END OUR DEFINES
********************************************************/

/********************************************************
* OUR FUNCTIONS
********************************************************/

/**
* @brief    The actual RPM shall be measured with an maximum measurement interval of 50ms
* @note     Issue #4
**/
double get_actual_rpm(const double* actual_rpm, double sim_time)
{
    static double current_actual_rpm = 0.0;

    if (((int)(sim_time * SEC_TO_US) % RPM_MEASUREMENT_INTERVAL) == 0) 
    {
        current_actual_rpm = *actual_rpm;
    }
    return current_actual_rpm;
}

/**
* @brief    The target RPM shall be measured with an maximum measurement interval of 50ms
* @note     Issue #3
**/
double get_target_rpm(const double *target_rpm, double sim_time)
{
    static double current_target_rpm = 0.0;
    if (((int)(sim_time*SEC_TO_US) % RPM_MEASUREMENT_INTERVAL) == 0) 
    {
        current_target_rpm = *target_rpm;
    }
    return current_target_rpm;
}

/**
* @brief    The temperature shall be measured with a maximum measurement interval of 100ms
* @note     Issue #5
**/
double get_temp(const double *temp, double sim_time)
{
    static double current_temp = 0.0;
    if (((int)(sim_time*SEC_TO_US) % TEMP_MEASUREMENT_INTERVAL) == 0) 
    {
        current_temp = *temp;
    }
    return current_temp;
}

/**
* @brief    The HALL1 sensor value shall be mesured with a maximum measurement inteval of 1µs
* @note     Issue #12
**/
bool get_hall1(const bool *h1)
{
    return *h1;
}
  
/**
* @brief    The Torque shall be measured with a maximum measurement interval of 10µs
* @note     Issue #13
**/
double get_torque(const double *torque, double sim_time)
{
    static double current_torque = 0.0;
    if (((int)(sim_time*SEC_TO_US) % TORQUE_MEASUREMENT_INTERVAL) == 0) 
    {
        current_torque = *torque;
    }
    return current_torque;
}

/**
* @brief    The system shall calculate the motor RPM using HALL1 sensor information 
*           and store it in the variable "Calculated_RPM"
* @note     RPM is calculated every time hall1 changes its state
* @note     Issue #15
**/
double get_calculated_rpm(bool h1)
{
    static double calculated_rpm = 0.0;
    static bool prev_h1 = 0;
    static double h1_time = 0.0;

    if(h1 != prev_h1)
    {
        prev_h1 = h1;
        if(h1)
        {
            //avoid division by 0
            if(h1_time == 0.0)
            {
                calculated_rpm = 0.0;
            } else {
                calculated_rpm = (60.0/(h1_time * US_TO_SEC * 2.0)); // *2 because we have 2 poles
            }
            h1_time = 0.0;
        }
    } else {
        h1_time += 1.0;
    }
    return calculated_rpm;
}

/**
* @brief    The system shall enter in STO_Mode if no reaction is measured on the actual RPM 
*           for more than 200ms after a change on target RPM 
* @note     Issue #6
**/
current_mode_t check_change_target_rpm_reaction_actual_rpm(current_mode_t current_mode, double actual_rpm, double target_rpm, double sim_time, FILE *file)
{
    static double last_actual_rpm = 0.0;
    static double last_target_rpm = 0.0;
    static double time_last_rpm_change = 0.0;
    static bool target_rpm_changed = false;

    if (actual_rpm == last_actual_rpm) 
    {
        if (((sim_time - time_last_rpm_change) > TRANSITION_TO_STO_RPM) && target_rpm_changed) {
            fprintf(file, "To STO mode, target RPM: %f has changed and not actual RPM %f for %fs\n", target_rpm, actual_rpm, sim_time - time_last_rpm_change);
            //reset the time spent when target rpm changes and actual rpm does not change for 200ms
            time_last_rpm_change = sim_time;
            target_rpm_changed = false;
            current_mode = STO_MODE;
        }
    } else {
        last_actual_rpm = actual_rpm;
        target_rpm_changed = false;
    }
    if (target_rpm != last_target_rpm) 
    {
        last_target_rpm = target_rpm;
        time_last_rpm_change = sim_time;
        target_rpm_changed = true;
    }
   return current_mode;
}

/**
* @brief    The system shall enter in STO_Mode if the 
*           temperature value is measured above 145°C for more than 100ms
* @note     Issue #10
**/     
current_mode_t check_temp_to_sto(current_mode_t current_mode, double temp, double sim_time, FILE *file)
{
    static double time_to_sto_temp = 0.0;
    if(temp > TEMP_TO_STO) 
    {
        if((sim_time - time_to_sto_temp) > TRANSITION_TO_STO_TEMP)
        {
            fprintf(file, "To STO, temperature is above 145 degrees: %f, for %fs\n", temp, sim_time - time_to_sto_temp);
            current_mode = STO_MODE;
            //reset the time spent when temp > 145°C for 100ms
            time_to_sto_temp = sim_time;
        }
    } else {
        time_to_sto_temp = sim_time;
    }
    return current_mode;
}

/**
* @brief    The system shall enter in STO_Mode if Start/Stop input changes to 
*           low but the RPM doesn't decrease of at least 50% in 2000ms
* @note     Issue #14
**/
current_mode_t check_change_button_reaction_rpm(current_mode_t current_mode, double actual_rpm, bool enable, FILE *file)
{
    static bool prev_enable = false;
    static bool time_ss_enable = false;
    static int time_ss = 0;
    static double ss_rpm = 0.0;

    if (enable != prev_enable)
    {
        if(!enable)
        {
            time_ss_enable = true;
            time_ss = 0;
            ss_rpm = actual_rpm;
        } else {
            time_ss_enable = false;
        }
    }
    prev_enable = enable;
    if (time_ss_enable) 
    {
        time_ss += 1;
        if ((time_ss > (TRANSITION_TO_STO_SS_BUTTON * SEC_TO_US)) && (actual_rpm > (0.5 * ss_rpm))) 
        {
            fprintf(file, "To STO mode, RPM does not decrease by half in %fs while the Start/Stop button changed to low\n", time_ss*US_TO_SEC);
            //reset the timer
            time_ss_enable = false;
            time_ss = 0;
            current_mode = STO_MODE;
        }
    }
    return current_mode;
}


/**
* @brief    The system shall enter in STO_Mode if "Calculated_RPM" and actual RPM 
*           have a deviation of more than 10%
* @note     we compare every  100ms the calculated rpm with the actual rpm 
*           (if we don't do that, we are alawys in STO_MODE because calculated_rpm doesn't 
*           have enough time to be calculated)
*           We changed it to 20% because the actual_rpm oscillate a lot
* @note     Issue #16
**/
current_mode_t check_deviation_calculated_actual_rpm(current_mode_t current_mode, double calculated_rpm, double actual_rpm, double sim_time, FILE *file)
{
    if((int)(sim_time*SEC_TO_US) % (10*RPM_MEASUREMENT_INTERVAL) == 0)
    {
        if((calculated_rpm < (0.8 * actual_rpm)) || (calculated_rpm > (1.2 * actual_rpm)))
        {
            fprintf(file, "To STO mode, deviation between calculated_rpm: %f, is more than 20 percents the actual_rpm: %f\n", calculated_rpm, actual_rpm);
            current_mode = STO_MODE;
        }
    }
    return current_mode;
}

/**
* @brief    The system shall enter in STO_Mode if oscillation of more than 5% is measured on Torque input for more than 1ms 
* @note     When implemented as asked, the system is constantly in STO mode
*           because the torque is always oscillating at 5% over 1ms!
*
*           Therefore, we chose our own implementation of issue 17:
*           If RPM is "constant" then check for oscillations
*           We assume that "constant" RPM happens 300 milliseconds after the last RPM change (STABILIZE_RPM)
*           Torque is measured every 10us, so we have 100 measurements in 1ms
*           We store the last 100 measurements in an array and check for oscillations
*
*           We assume the average torque is 0.0
*           Oscillations are detected if the highest torque value
*           AND the lower torque values are more/less than 0.008% of the actual RPM
*           This constant is necessary as torque greatly decreases with higher RPM
*           therefore we need to adjust the RPM value that we are comparing to 
* @note     Issue #17
**/
current_mode_t check_oscillation_torque(current_mode_t current_mode, double torque, double actual_rpm, double target_rpm, double sim_time, FILE *file)
{
    static double torque_array[100];
    static uint8_t torque_array_index = 0;
    static bool torque_array_is_full = false;
    static double last_target_rpm = 0.0;
    static double time_last_rpm_change = 0.0;

    if((last_target_rpm == target_rpm) )
    {
        if ((sim_time - time_last_rpm_change) > STABILIZE_RPM)
        {
                if (((int)(sim_time*SEC_TO_US) % TORQUE_MEASUREMENT_INTERVAL) == 0) 
            {
                if (torque_array_index < 99) 
                {
                    torque_array[torque_array_index] = torque;
                    ++torque_array_index;
                } else {
                    // Array is full, replace the first values with new ones
                    torque_array_index = 0;
                    torque_array[torque_array_index] = torque;
                    torque_array_is_full = true;
                }
            }
            if (torque_array_is_full) 
            {
                // Find the highest and lowest torque values
                float highest_torque = torque_array[0];
                float lowest_torque = torque_array[0];

                for (int i = 0; i < 100; i++) {
                    if (torque_array[i] > highest_torque) 
                    {
                        highest_torque = torque_array[i];
                    }
                    if (torque_array[i] < lowest_torque) 
                    {
                        lowest_torque = torque_array[i];
                    }
                }
                // Check if the highest and lowest torque values meet the condition (average torque = 0)
                if ((highest_torque > (COEFF_RPM * actual_rpm)) && (lowest_torque < -(COEFF_RPM * actual_rpm))) 
                {
                    fprintf(file, "To STO mode, oscillation occurs, highest torque: %f, lowest torque: %f\n", highest_torque, lowest_torque);
                    //reset the time spent when torque oscillates for 1ms
                    time_last_rpm_change = sim_time;
                    current_mode = STO_MODE;
                }
            }
        } 
    } else {
        torque_array_is_full = false;
        for(int i = 0; i < 100; i++)
        {
            torque_array[i] = 0.0;
        }
        torque_array_index = 0;
        last_target_rpm= target_rpm;
        time_last_rpm_change= sim_time;
    }
    return current_mode;
}
           
/**
* @brief    The system shall enter in DEGRADED_Mode if the temperature value 
*           is measured above 100°C for more than 100ms
* @note     Issue #7
**/
current_mode_t check_temp_to_deg(current_mode_t current_mode, double temp, double sim_time, FILE *file)
{
    static double time_run_to_deg_temp = 0.0;
    if(temp > TEMP_RUNNING_TO_DEGRADED)
    {
        if((sim_time - time_run_to_deg_temp) > TRANSITION_RUNNING_TO_DEGRADED_TEMP)
        {
            fprintf(file, "RUNNING to DEGRADED mode, temperature is above 100 degrees: %f, for %fs\n", temp, sim_time - time_run_to_deg_temp);
            //reset the time spent when temp > 100°C for 100ms
            time_run_to_deg_temp = sim_time;
            current_mode = DEGRADED_MODE;

        }
    } else {
        time_run_to_deg_temp = sim_time;
    }
    return current_mode;
}

/**
* @brief    The system shall enter in STO_Mode after spending 2000ms in DEGRADED_Mode
* @note     Issue #8
**/
current_mode_t check_to_sto(current_mode_t current_mode, double current_degraded_time, double sim_time, FILE *file)
{
    if((sim_time - current_degraded_time) >= TRANSITION_MODE)
    {
        fprintf(file, "DEGRADED to STO because we spent %fs in DEGRADED\n", sim_time - current_degraded_time);
        current_mode = STO_MODE;
    }
    return current_mode;
}

/**
* @brief    The system shall enter in RUNNING_Mode when it is in DEGRADED_Mode and 
*           the temperature is measured below 90°C during 200ms
* @note     Issue #9
**/
current_mode_t check_temp_to_run(current_mode_t current_mode, double temp, double sim_time, FILE *file)
{
    static double time_deg_to_run_temp = 0.0;
    if(temp < TEMP_DEGRADED_TO_RUNNING) 
    {
        if((sim_time - time_deg_to_run_temp) > TRANSITION_DEGRADED_TO_RUNNING_TEMP)
        {
            fprintf(file, "DEGRADED to RUNNING, temperature is below 90 degrees: %f for %fs\n", temp, sim_time - time_deg_to_run_temp);
            //rest the time spent when temp < 90°C for 200ms
            time_deg_to_run_temp = sim_time;
            current_mode = RUNNING_MODE;
        }
    } else {
        time_deg_to_run_temp = sim_time;
    }
    return current_mode;
}

/**
* @brief    When the system is in DEGRADED_Mode, the target RPM shall be limited to 4000 rpm
* @note     Issue #11
**/
double limit_rpm(double *target_rpm)
{
     if (*target_rpm > TARGET_RPM_DEGRADED_LIMIT)
    {
        *target_rpm = TARGET_RPM_DEGRADED_LIMIT;
    }
    return *target_rpm;
}
               
/**
* @brief    For testing purpose, when the system is in STO_Mode for 2000ms, 
*           it shall enter in RUNNING_Mode
* @note     Issue #19
**/
current_mode_t check_to_run(current_mode_t current_mode, double current_sto_time, double sim_time, FILE *file) 
{
    if((sim_time - current_sto_time) >= TRANSITION_MODE)
    {
        fprintf(file, "STO to RUNNING because we spent %fs in STO\n", sim_time - current_sto_time);
        current_mode = RUNNING_MODE;
    }
    return current_mode;
}

/********************************************************
* END OUR FUNCTIONS
********************************************************/

void *run_trapezoidal_controller(void *arg)
{
    int status = SUCCESS;
    BldcControllerSimulatorWindow *win;
    controllerIo *controller_io;
    motorQuantities motor_quant;
    pwmMosfets bridge_signals;
    hallSensors hall_sensors;
    bool sto_active_low;
    bool enable;
    double sim_time = 0.0;
    double sim_duration;
    int scenario;

    /********************************************************
    * OUR VARIABLES
    ********************************************************/
    
    /**
     * @brief   Track all mode changes of the simulation in  a file
     * @note    every mode changes is written in the file mode_changed.txt using fprintf function from stdio library
     * @note    Issue #18
    **/
   //open the file in write mode
    FILE *file = fopen("../../src/mode_changed.txt", "w");

    //check if the file opened successfully
    if (file == NULL) {
        printf("Error opening file!\n");
    } 

    //variable for the mode management
    current_mode_t current_mode = RUNNING_MODE;
    current_mode_t previous_mode = NONE;

    //variable to store time spent in each mode
    double current_sto_time = 0.0;   
    double current_degraded_time = 0.0;

    //variable to store the motor informations

    //sensors measurements
    double torque = 0.0;
    double temp = 0.0;
    bool hall1 = 0;
    double target_rpm = 0.0;
    double actual_rpm = 0.0;
    double calculated_rpm = 0.0; 

    
    /********************************************************
    * END OUR VARIABLES
    ********************************************************/

    win = arg;
    controller_io = &(win->controller_io);

    /* Get initial values */
    g_mutex_lock(controller_io->mutex);
    motor_quant.actual_rpm = controller_io->motor_quant.actual_rpm;
    motor_quant.target_rpm = controller_io->motor_quant.target_rpm;
    motor_quant.temp = controller_io->motor_quant.temp;
    sto_active_low = controller_io->sto_active_low;
    sim_duration = controller_io->sim_duration;
    enable = controller_io->is_enabled;
    scenario = controller_io->scenario;
    g_mutex_unlock(controller_io->mutex);

    printf("Sim duration: %f\n", sim_duration);

    /* Setup */
    pthread_cleanup_push(sim_cleanup, NULL);
    log_init();

    status = run_simulation_step(&bridge_signals, sto_active_low, scenario, 0, &motor_quant, &hall_sensors);
    if (status != SUCCESS)
    {
        sim_time = sim_duration;
    }

    while (sim_time < sim_duration)
    {
        status = run_pid(motor_quant.target_rpm, motor_quant.actual_rpm, &bridge_signals.pwm);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_inverter_mucontroller(&hall_sensors, &bridge_signals, sto_active_low);
        if (status != SUCCESS)
        {
            break;
        }

        status = run_simulation_step(&bridge_signals, sto_active_low, scenario, sim_time, &motor_quant, &hall_sensors);
        if (status != SUCCESS)
        {
            break;
        }

        if (LOG_STUDENT_IO)
        {
            log_student_io(&bridge_signals, sto_active_low, &motor_quant, &hall_sensors, enable, sim_time);
        }

        sim_time += RK_NUMBER_STEPS * RK_STEP_SIZE;

        /* Update controller_io */
        g_mutex_lock(controller_io->mutex);
        controller_io->sim_time = sim_time;
        controller_io->motor_quant.actual_rpm = motor_quant.actual_rpm;

        motor_quant.target_rpm = controller_io->motor_quant.target_rpm;
        enable = controller_io->is_enabled;
        g_mutex_unlock(controller_io->mutex);

        pthread_testcancel();
        g_idle_add(update_values_gui, win);


        /********************************************************
        * OUR CODE
        ********************************************************/
        /**
        * @brief    The safety-related code shall not interfere with 
        *           the motor control when system is in RUNNING_Mode
        * @note     The way the code as been implemented with the switch case,
        *           the safety-related code doesn't interfere with the motor control
        *           while in RUNNING_Mode
        * @note     Issue #1
        **/

        //actual_rpm measurement
        actual_rpm = get_actual_rpm(&motor_quant.actual_rpm, sim_time);

        //target_rpm measurement
        target_rpm = get_target_rpm(&motor_quant.target_rpm, sim_time);

        //temperature measurement
        temp = get_temp(&motor_quant.temp, sim_time);

        //hall1 sensor measurement
        hall1 = get_hall1(&hall_sensors.h1);

        //torque measurement
        torque = get_torque(&motor_quant.torque, sim_time);

        //calculate rpm
        calculated_rpm = get_calculated_rpm(hall1);

        //mode management
        if(current_mode != STO_MODE)
        {
            //rpm check
            current_mode = check_change_target_rpm_reaction_actual_rpm(current_mode, actual_rpm, target_rpm, sim_time, file);

            //temp check
            current_mode = check_temp_to_sto(current_mode, temp, sim_time, file);

            //button check
            current_mode = check_change_button_reaction_rpm(current_mode, actual_rpm, enable, file);

            //deviation check
            current_mode = check_deviation_calculated_actual_rpm(current_mode, calculated_rpm, actual_rpm, sim_time, file);

            //oscillation check 
            current_mode = check_oscillation_torque(current_mode, torque, actual_rpm,target_rpm, sim_time, file);
        }

        switch (current_mode)
        {
            case RUNNING_MODE:
                if(previous_mode != current_mode)
                {
                    sto_active_low = true;
                    fprintf(file, "RUNNING MODE\n");
                    previous_mode = current_mode;
                }
                //temp check
                current_mode = check_temp_to_deg(current_mode, temp, sim_time, file);

                break;
            case DEGRADED_MODE:
                if(previous_mode != current_mode)
                {
                    sto_active_low = true;
                    current_degraded_time = sim_time;
                    fprintf(file, "DEGRADED MODE\n");
                    previous_mode = current_mode;
                }

                //sto check
                current_mode = check_to_sto(current_mode, current_degraded_time, sim_time, file);

                //temp check
                current_mode = check_temp_to_run(current_mode, temp, sim_time, file);

                //rpm limitation
                motor_quant.target_rpm = limit_rpm(&motor_quant.target_rpm);
            
                break;
            case STO_MODE:
                if(previous_mode != current_mode)
                {
                    sto_active_low = false;
                    current_sto_time = sim_time;
                    fprintf(file, "STO MODE\n");
                    previous_mode = current_mode;
                }
                //run check
                current_mode = check_to_run(current_mode, current_sto_time, sim_time, file);

                break;
            case NONE:
            default:
                sto_active_low = false;
                break;
        }
    }
    //close the file
    fclose(file);

    pthread_cleanup_pop(TRUE); /* Executes cleanup function */

    return NULL;
}

int update_values_gui(void *arg)
{
    double sim_time;
    double actual_rpm;
    BldcControllerSimulatorWindow *win;

    win = arg;

    g_mutex_lock(win->controller_io.mutex);
    sim_time = win->controller_io.sim_time;
    actual_rpm = win->controller_io.motor_quant.actual_rpm;
    g_mutex_unlock(win->controller_io.mutex);

    gtk_level_bar_set_value(GTK_LEVEL_BAR(win->sim_time_level_bar), sim_time);
    gtk_level_bar_set_value(GTK_LEVEL_BAR(win->actual_level_bar), actual_rpm);
    set_actual_rpm_label(win, actual_rpm);

    return false; /* For use with glib_idle_add (remove after execution) */
}

int set_initial_values_controller_io(BldcControllerSimulatorWindow *win)
{
    int status = SUCCESS;
    double sim_duration = g_ascii_strtod(gtk_editable_get_text(GTK_EDITABLE(win->sim_duration_entry)), NULL);

    if (sim_duration == 0.0)
    {
        sim_duration = g_ascii_strtod(gtk_entry_get_placeholder_text(GTK_ENTRY(win->sim_duration_entry)), NULL);
    }

    gtk_level_bar_set_max_value(GTK_LEVEL_BAR(win->sim_time_level_bar), sim_duration);

    g_mutex_lock(win->controller_io.mutex);
    win->controller_io.motor_quant.actual_rpm = 0.0;
    win->controller_io.motor_quant.torque = 0.0;
    win->controller_io.motor_quant.temp = 25.0;
    win->controller_io.sto_active_low = TRUE;
    win->controller_io.sim_duration = sim_duration;
    win->controller_io.sim_time = 0.0;
    win->controller_io.sto_active_low = TRUE;
    g_mutex_unlock(win->controller_io.mutex);

    return status;
}

void set_actual_rpm_label(BldcControllerSimulatorWindow *win, double raw_val)
{
  double rounded_value;
  gchar *val_str;

  rounded_value = round(raw_val / 10.0) * 10.0;

  val_str = g_strdup_printf("%.0f", rounded_value);

  gtk_label_set_text(GTK_LABEL(win->actual_value_label), val_str);
}

void sim_cleanup()
{
    log_close();
    /* Calling g_mutex_unlock on unlocked g_mutex is undefined behaviour! */
}