# BLDC Controller and Simulator

This will be the basis for the practical exercices for the Software Architecture Course at EPFL.

Your code can be added to the function `run_trapezoidal_controller()` in the file students_code.c. The function is called when the simulation is started and then the while loop inside the function runs for the duration of the simulation. You are free to create your own functions in that file as well (if you want to for structuring your code).
Apart from students_code.c (and students_code.h if you create your own functions), the files should not need changes from you. If you want to create additional files, don't forget to add the source files to the target in the CMakeLists.txt file located in the source folder.

## Run on Windows natively

Note: You need the second version of bldc_controller_simulator.

1) Install MSYS2, a command prompt (Recommended by VScode docs, [source](https://code.visualstudio.com/docs/cpp/config-mingw)):

    Installer can be found at: https://github.com/msys2/msys2-installer/releases/download/2024-01-13/msys2-x86_64-20240113.exe

2) Open MSYS2 and install mingw with the command:

    ```
    pacman -S --needed base-devel mingw-w64-ucrt-x86_64-{toolchain,cmake,ninja}
    ```

3) Add the mingw installation to the Windows PATH variable (source vscode doc as above):

    Find the location of the mingw installation with `where gcc`. (If it doesn't find it even after installation, you may need a reboot.)

    1) In the Windows search bar, type Settings to open your Windows Settings.
    2) Search for Edit environment variables for your account.
    3) In your User variables, select the Path variable and then select Edit.
    4) Select New and add the MinGW-w64 destination folder you recorded during the installation process to the list. The entry should end in ..\bin\ (do not add gcc.exe).
    5) Select OK to save the updated PATH. You will need to reopen any console windows for the new PATH location to be available.

4) Install gtk4 package in MSYS2:

    ```pacman -S mingw-w64-ucrt-x86_64-gtk4```

5) In a command prompt, open in the bldc_controller_simulator folder (which contains the folders src, include, the readme file etc.), set up the cmake build system with:

    ```cmake -D CMAKE_BUILD_TYPE=DEBUG -G "Ninja" -S . -B build```

6) Compile the simulator with:

    ```cmake --build build```


## Run on Windows via WSL

1) If Ubuntu 22.04 is not yet installed in WSL:

    ```wsl --install -d Ubuntu-22.04```

2) On Windows, go to folder bldc_controller_simulator-c6e8a8631894a67aadc6625c126b2164e6a0c89c (inside this folder are the folders src, include, le README etc) and open a terminal in this folder. Run the command:

    ```wsl -d Ubuntu-22.04```

3) You have now a command line interface inside Linux.

4) Install gtk4 development library and cmake:

    ```
    sudo apt update # Update package index
    sudo apt install -y libgtk-4-dev
    sudo apt install -y cmake
    ```

5) You are now ready to launch the program, with the command below. If you want to relaunch wsl again later, repeat only step 2).

    ```cmake -D CMAKE_BUILD_TYPE=DEBUG -S . -B build && cmake --build build```

6) The executable is build/src/bldc_controller_simulator and can be launched as follows:

    ```
    cd build/src
    ./bldc_controller_simulator
    ```

## Run on Ubuntu 22.04

Note: It does not run on Ubuntu 20.04

1) Install cmake and development library for gtk4:

    ```
    sudo apt update # Update package index
    sudo apt install -y libgtk-4-dev
    sudo apt install -y cmake
    ```
2) To compile the program, go to the project folder containing the src, include, README, etc and run:

    ```cmake -D CMAKE_BUILD_TYPE=DEBUG -S . -B build && cmake --build build```

3) To run the program:

    ```
    cd build/src
    ./bldc_controller_simulator
    ```

## Run on Mac OSX

Students feedback was integrated into this version of the program and it *should* work on Mac OSX. However, this was *not* tested.

## Note on version compatability with the different OSes

The first version your received on the first day (bldc_controller_simulator-c6e8a8631894a67aadc6625c126b2164e6a0c89c) works on Ubuntu 22.04 and with Windows via WSL.

The second version is verified to work on Ubuntu 22.04, Windows via WSL and natively on Windows (with mingw gcc compiler). It also contains a fix for the warnings given for the returns in the students_code.c file. The fix is not mandatory.

## Cppcheck for MISRA C compliance

The open source version was used, therefore only **MISRA C 2012 - amendment #2** is included.

Launch the check from the top project folder with the following command:

```cppcheck --cppcheck-build-dir=build --enable=all --suppress=missingIncludeSystem --inconclusive --language=c -I include src```

Note: Project must be built before check with above command, as it only checks source files which are changed in the build folder.

Note: Unmatched suppression: missingIncludeSystem shown but it has the intended effect of removing warnings of stdlibs not found.

## Testing with CUnit

test_c_unit target built with CMake. Run tests by executing the target ./build/tests/test_c_unit

https://cunit.sourceforge.net/


## Notes

CMake structure built to example: https://gitlab.com/CLIUtils/modern-cmake/-/tree/master/examples/extended-project


## Sources

The mathematical model is taken from the following papers:

S. Mondal, A. Mitra and M. Chattopadhyay, "Mathematical modeling and simulation of Brushless DC motor with ideal Back EMF for a precision speed control," 2015 IEEE International Conference on Electrical, Computer and Communication Technologies (ICECCT), Coimbatore, India, 2015, pp. 1-5, doi: 10.1109/ICECCT.2015.7225944. keywords: {MATLAB;Silicon;Sensors;Load modeling;Shafts;Switches;Brushless DC motor;Back EMF;Hall sensors;PI controller;electromagnetic torque},

https://core.ac.uk/download/pdf/53188902.pdf

https://koreascience.kr/article/JAKO201117148820110.pdf

https://www.matec-conferences.org/articles/matecconf/pdf/2017/53/matecconf_icmite2017_00172.pdf

https://www.ijert.org/research/mathematical-modeling-of-brushless-dc-motor-and-its-speed-control-using-pi-controller-IJERTV8IS050446.pdf

https://www.codesansar.com/numerical-methods/ordinary-differential-equation-using-runge-kutta-rk-method-using-c-programming.htm