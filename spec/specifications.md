# Specifications

### 1. Fonctions principales


### 2. Safety goal
* Maintenir le moteur dans un état stable
    * Le système doit être conçu pour détecter toute condition susceptible de compromettre la stabilité du moteur BLDC.
    * En cas de détection d'une anomalie ou d'un dysfonctionnement critique, le système doit prendre des mesures correctives pour restaurer la stabilité du moteur.

### 3. Exigences de sécurité
 * Arrêt d'urgence en cas de surchauffe :
    * Le système doit être capable de détecter une surchauffe du moteur et déclencher un arrêt d'urgence pour éviter tout risque de dommage.
 * Surveillance des courants anormaux :
    * Le système doit surveiller en permanence les courants électriques et réagir en cas de détection de courants anormaux, évitant ainsi les situations dangereuses.

### 4. Exigences techniques
* Précision de la régulation de vitesse :
    * La régulation de vitesse du moteur doit avoir une précision de ±5% pour garantir un fonctionnement stable.
* Plage de température de fonctionnement :
    * Le moteur doit être capable de fonctionner de manière fiable dans une plage de températures allant de -10°C à 50°C.

### 5. Exigences logicielles
* Algorithme de régulation PID :
    * Le logiciel doit implémenter un algorithme de régulation PID (Proportionnel, Intégral, Dérivé) pour assurer une régulation précise de la vitesse.
* Démarrage progressif :
    * Le logiciel doit inclure un algorithme de démarrage progressif pour éviter les pics de courant lors du démarrage du moteur.

### 6. Tests cases
* Test de surchauffe :
    * Simuler une surchauffe et vérifier que le système déclenche un arrêt d'urgence pour maintenir la stabilité du moteur.
* Test de charge maximale :
    * Soumettre le moteur à sa charge maximale spécifiée et vérifier que la régulation de vitesse maintient la stabilité du moteur dans cette condition.

